<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\LoginController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'showAll'])->name('home');
Route::get('category/{slug}', [PostController::class, 'showArticle'])->name('showArticle');
Route::get('{slug}', [CategoryController::class, 'showCategory'])->name('showCategory');

Route::prefix('user')->group(function () {
	
	Route::get('registration', [RegistrationController::class, 'showForm'])->name('registration');
	Route::post('save', [RegistrationController::class, 'save'])->name('create_user');

	Route::get('profile', function () {
		return view('pages.profile');
	})->middleware('auth')->name('profile');

	Route::get('login', [LoginController::class, 'showForm'])->name('login');
	Route::post('check', [LoginController::class, 'check'])->name('login_check');

	Route::get('logout', [LoginController::class, 'logout'])->name('logout');
});


