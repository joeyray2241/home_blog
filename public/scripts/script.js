let reg_sub = document.querySelector('#reg_sub');
reg_sub.disabled = true;

document.querySelector('#check_pass').addEventListener('keyup', function () {
	let pass = document.querySelector('#pass').value;
	if( pass == this.value) {
		reg_sub.disabled = false;
	} else {
		reg_sub.disabled = true;
	}
});


document.querySelector('#pass').addEventListener('keyup', function () {
	let check_pass = document.querySelector('#check_pass').value;
	if( check_pass == this.value) {
		reg_sub.disabled = false;
	} else {
		reg_sub.disabled = true;
	}
});