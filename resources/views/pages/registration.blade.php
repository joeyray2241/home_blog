@extends('layouts.main')

@section('title', 'Регистрация')

@section('content')
	<h1>Регистрация</h1>
	<form method="post" action="{{route('create_user')}}">
		@csrf
		<p>Имя: <input type="text" name="name"></p>
		<p>Фамилия: <input type="text" name="lastname"></p>
		<p>E-mail: <input type="text" name="email"></p>
		<p>Пароль: <input type="password" name="password" id="pass"></p>
		<p>Проверка пароля: <input type="password" name="check_pass" id="check_pass"></p>
		<input type="submit" id="reg_sub">
	</form>
@endsection
