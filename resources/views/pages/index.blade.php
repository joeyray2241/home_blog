@extends('layouts.main')
@section('title', 'HOME')
@section('content')
	<h1>{{$title}}</h1>
	<div class="wrapper">
		@foreach($posts as $post)
			<div class="post">
				<h3>
					<a href="{{route('showArticle', $post->slug)}}">{{$post->title}}</a>
					<a href="{{route('showCategory', $post->category['slug'])}}">{{$post->category['title']}}</a>
				</h3>
				<p>{{$post->description}}</p>
			</div>
		@endforeach
		<div class="pagination">
			{{$posts->links('vendor.pagination.bootstrap-4')}}
		</div>
	</div>
@endsection