@extends('layouts.main')
@section('title', 'Вход')

@section('content')
	<h1>Авторизация</h1>
	<form method="post" action="{{route('login_check')}}">
		@csrf
		<p>E-mail: <input type="text" name="email"></p>
		<p>Password: <input type="password" name="password"></p>
		<p><input type="submit"></p>
	</form>
@endsection