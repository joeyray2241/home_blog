<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="/styles/style.css">
	<script src="/scripts/script.js" defer></script>
	<title>@yield('title')</title>
</head>
<body>
	<div class="header">
		<a href="{{route('home')}}">главная</a>
		<a href="{{route('login')}}">вход</a>
		<a href="{{route('registration')}}">регистрация</a>
		@yield('logout')
		<a href="">профиль</a>
	</div>
	<div class="menu">
		@yield('menu')
	</div>
	<div class="content">
		@yield('content')
	</div>
</body>
</html>