<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for($i = 1; $i <= 30; $i++) {
        	DB::table('posts')->insert([
        		'title' => 'Статья #'.$i,
        		'description' => 'Описание статьи #'.$i,
        		'text' => 'Made this Lyric Video for "One Night In Tokyo" from the album "Dark Connection" (2021)!!\nWe do not own the rights to the audio, or the pictures!\nEnjoy!!',
        		'slug' => 'post-'.$i,
        		'category_id' => rand(1,5)

        	]);
        }
    }
}
