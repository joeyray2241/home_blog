<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public function showCategory($slug) {
    	$category = Category::all();
    	$current_category = Category::where('slug', $slug)->first();

    	return view('pages.index', [
    		'category' => $category,
    		'posts' => $current_category->posts()->paginate(5),
    		'title' => $current_category->title
    	]);
    }
}
