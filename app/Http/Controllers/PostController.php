<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class PostController extends Controller
{
    public function showAll() {
    	$posts = Post::paginate(5);

    	return view('pages.index', [
    		'posts' => $posts,
    		'title' => 'Главная страница'
    	]);
    }

    public function showArticle($slug) {
    	$post = Post::where('slug', $slug)->first();

    	return view('pages.article', [
    		'post' => $post
    	]);
    }
}
