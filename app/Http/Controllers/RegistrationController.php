<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class RegistrationController extends Controller
{
    public function showForm() {
    	if(Auth::check()) {
    		return redirect(route('profile'));
    	}

    	return view('pages.registration');
    }

    public function save(Request $request) {
    	$fields = $request->validate([
    		'name' => 'required',
    		'lastname' => 'required',
    		'email' => 'required|email',
    		'password' => 'required'
    	]);

    	$user = User::create($fields);

    	if($user) {
    		Auth::login($user);
    		return redirect(route('profile'));
    	}
    }
}
